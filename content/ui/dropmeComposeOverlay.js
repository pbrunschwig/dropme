/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


"use strict";

/*globally available Thunderbird variables/object/functions: */
/*global gMsgCompose: false, getCurrentIdentity: false, gNotification: false */
/*global UpdateAttachmentBucket: false, gContentChanged: true */
/*global AddAttachments: false, AddAttachment: false, ChangeAttachmentBucketVisibility: false, GetResourceFromUri: false */
/*global Recipients2CompFields: false, Attachments2CompFields: false, DetermineConvertibility: false, gWindowLocked: false */
/*global CommandUpdate_MsgCompose: false, gSMFields: false, setSecuritySettings: false, getCurrentAccountKey: false */
/*global Sendlater3Composing: false, MailServices: false */
/*global dump: false */
/*eslint no-invalid-this: 0*/


var Cc = Components.classes;
var Ci = Components.interfaces;

function DEBUG(msgStr) {
  // try {
  //   var consoleSvc = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);
  //   var scriptError = Cc["@mozilla.org/scripterror;1"].createInstance(Ci.nsIScriptError);
  //   scriptError.init(msgStr, null, null, 0, 0, scriptError.errorFlag, "DropMe");
  //   consoleSvc.logMessage(scriptError);
  // }
  // catch (ex) { }

  dump(msgStr);
}

class DropmeRcptEditor {

  /**
   * 
   * @param {String} editFieldId 
   * @param {String} rcptType: one of to, cc, bcc
   * @param {*} recipientsList 
   */
  constructor(editFieldId, rcptType, recipientsList = null) {
    this.editFieldId = editFieldId;
    this.rcptType = rcptType;
    this.editField = document.getElementById(this.editFieldId);

    this.recipients = [];
    this.maxInputId = 0;

    this.compFields = gMsgCompose.compFields;

    if (!this.compFields) {
      this.compFields = Cc["@mozilla.org/messengercompose/composefields;1"].createInstance(Ci.nsIMsgCompFields);
    }

    if (recipientsList) {
      this.appendRecipients(recipientsList);
    }
    this.addEditorField(true);
  }

  /**
   * Populate the recipients
   * @param {String} list of recipients, separated by space, semicolons etc
   */
  appendRecipients(recipientsList) {
    this.convertToRecipient(recipientsList);
  }

  appendRecipient(email, displayName, beforeNode) {
    this.addEditorField(false, beforeNode);
    let self = this;

    let node = document.createElement("label");
    node.setAttribute("value", displayName);
    node.setAttribute("class", "dropmeRecipient");
    node.setAttribute("email", email);
    node.setAttribute("tooltiptext", email);

    if (!beforeNode) {
      this.editField.appendChild(node);
    } else {
      this.editField.insertBefore(node, beforeNode);
    }

    node.addEventListener("dblclick", (event) => {
      event.stopPropagation();
      event.preventDefault();

      let editValue = self.getDisplayEmail(displayName, email);
      let prevEntryField = node.nextSibling.firstChild;
      prevEntryField.value = editValue;
      prevEntryField.style.width = "60px";
      node.parentNode.removeChild(node.previousSibling);
      node.parentNode.removeChild(node);

      self.calculateRequiredWidth(prevEntryField, editValue, 200);
      prevEntryField.focus();
    });
  }

  getDisplayEmail(displayName, email) {
    let res = displayName ? displayName : "";

    if (res) {
      res += ` <${email}>`;
    }
    else 
      res = email;

    return res;
  }

  convertToRecipient(node) {
    let adressList;
    if (typeof (node) === "string") {
      adressList = MailServices.headerParser.makeFromDisplayAddress(node, {});
      node = null;
    } else
      adressList = MailServices.headerParser.makeFromDisplayAddress(node.value, {});

    for (let i in adressList) {
      let displayName = "",
        email = "";

      if (adressList[i].email !== "") {
        email = adressList[i].email.trim();
        if (adressList[i].name !== "") {
          displayName = adressList[i].name.trim();
        } else {
          displayName = email;
        }
        this.appendRecipient(email, displayName, node ? node.parentNode : null);
      }
    }

    if (node) node.value = "";
  }

  focusNext() {
    let inputNodes = Array.prototype.slice.call(
      document.querySelectorAll(['input', 'editor'])
    );

    let currInput = document.activeElement;
    let nextInputIndex = inputNodes.indexOf(currInput) + 1;
    while (nextInputIndex < inputNodes.length) {
      if (currInput.parentNode === inputNodes[nextInputIndex].parentNode) {
        nextInputIndex++;
      } else {
        break;
      }
    }

    let input = inputNodes[nextInputIndex];
    input.focus();
  }

  addEditorField(isLast, beforeNode) {
    let self = this;
    ++this.maxInputId;

    // <div class="autocomplete" style="width:300px;">
    // <input id="myInput" type="text" name="myCountry" placeholder="Country">
    // </div>

    let autocompleteNode = document.createElementNS("http://www.w3.org/1999/xhtml", "html:span");
    autocompleteNode.setAttribute("class", "dropmeAutocomplete");

    let node = document.createElementNS("http://www.w3.org/1999/xhtml", "html:input");
    node.setAttribute("class", "dropmeEdit");
    node.setAttribute("lastEntry", isLast);
    node.style.border = "none";
    node.style.background = "white";
    node.id = "dropmeInput-" + this.maxInputId;

    autocompleteNode.appendChild(node);
    this.prepareAddressCompletion(node);

    node.addEventListener('keypress', (event) => {
      if (event.key === "Enter" || event.key === "Tab") {
        self.closeAllAutocompletionLists();
        if (node.value !== "") {
          self.convertToRecipient(node);
        } else {
          self.focusNext();
        }

        event.stopPropagation();
        event.preventDefault();
        return;
      }
    });

    if (!beforeNode) {
      this.editField.appendChild(autocompleteNode);
    } else {
      this.editField.insertBefore(autocompleteNode, beforeNode);
    }

    let minWidth = isLast ? 200 : 1;

    node.style.width = minWidth + 'px';
    node.onkeypress = node.onkeydown = node.onkeyup = function () {
      let input = this;
      setTimeout(() => {
        self.calculateRequiredWidth(input, input.value, minWidth);
      }, 1);
    };
  }

  calculateRequiredWidth(node, text, minWidth) {
    const maxWidth = 300,
      pad_right = 1;

    let tmp = document.createElement('div');
    tmp.style.padding = '0';
    tmp.style.cssText = getComputedStyle(node, null).cssText;
    if (node.currentStyle)
      tmp.style.cssText = node.currentStyle.cssText;
    tmp.style.width = '';
    tmp.style.position = 'absolute';
    tmp.innerHTML = text.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;")
      .replace(/ /g, "&#160;");
    node.parentNode.appendChild(tmp);
    let width = tmp.clientWidth + pad_right + 1;
    tmp.parentNode.removeChild(tmp);
    if (minWidth <= width && width <= maxWidth) {
      node.style.width = width + 'px';
    }
  }

  prepareAddressCompletion(inputNode) {
    const self = this;

    inputNode.addEventListener("input", function (event) {
      let lineElem, i, searchTerm = this.value;

      self.closeAllAutocompletionLists();
      if (!searchTerm) {
        return;
      }

      const boundingRect = inputNode.getBoundingClientRect();

      let listElem = document.createElement("listbox");
      this.parentNode.appendChild(listElem);
      listElem.setAttribute("id", this.id + "dropmeAutocomplete-list");
      listElem.setAttribute("class", "dropmeAutocomplete-list");
      listElem.style.left = boundingRect.left + "px";

      let matches = self.matchAddresses(searchTerm);

      if (!matches.length) {
        self.closeAllAutocompletionLists();
        return;
      }

      for (i in matches) {
        lineElem = listElem.appendItem(matches[i], matches[i]);

        lineElem.addEventListener("click", function (event) {
          inputNode.value = listElem.getSelectedItem(0).value;
          self.closeAllAutocompletionLists();
          self.convertToRecipient(inputNode);
        });
      }

      listElem.setAttribute("rows", Math.min(matches.length, 4));
      setTimeout(() => {
        for (let i = 0; i < listElem.itemCount; i++) {
          let l = listElem.getItemAtIndex(i);
          l.setAttribute("class", "dropmeAutocomplete-item");
        }

      }, 1);
    });

    inputNode.addEventListener("keydown", function (event) {
      let listElem = document.getElementById(this.id + "dropmeAutocomplete-list");

      switch (event.keyCode) {
        case 40: // DOWN key
          if (listElem.selectedIndex < listElem.itemCount) {
            ++listElem.selectedIndex;
            listElem.ensureIndexIsVisible(listElem.selectedIndex);
          }
          break;
        case 38: //UP key
          if (listElem.selectedIndex > 0) {
            --listElem.selectedIndex;
            listElem.ensureIndexIsVisible(listElem.selectedIndex);
          }
          break;
        case 13: // ENTER
          if (listElem) {
            listElem.getSelectedItem(0).click();
            event.preventDefault();
          }
      }
    });

    /* close all lists if user clicks elsewhere in the document */
    document.addEventListener("click", function (e) {
      self.closeAllAutocompletionLists(e.target);
    });
  }

  matchAddresses(searchTerm) {
    let abManager = Cc["@mozilla.org/abmanager;1"].getService(Ci.nsIAbManager);

    let allAddressBooks = abManager.directories;
    let matches = [];
    searchTerm = searchTerm.toLowerCase();

    while (allAddressBooks.hasMoreElements()) {
      let addressBook = allAddressBooks.getNext().QueryInterface(Ci.nsIAbDirectory);

      if (addressBook instanceof Ci.nsIAbDirectory) { // or nsIAbItem or nsIAbCollection
        // ask for confirmation for each address book:

        let allChildCards = addressBook.childCards;

        while (allChildCards.hasMoreElements()) {

          let card = allChildCards.getNext().QueryInterface(Ci.nsIAbCard);

          if (card.primaryEmail.indexOf("@") < 0) {
            continue;
          }

          if (card.displayName.toLowerCase().indexOf(searchTerm) >= 0 ||
            card.primaryEmail.toLowerCase().indexOf(searchTerm) >= 0 ||
            card.firstName.toLowerCase().indexOf(searchTerm) >= 0 ||
            card.lastName.toLowerCase().indexOf(searchTerm) >= 0) {

            let goodMatch = (card.displayName.toLowerCase().indexOf(searchTerm) === 0 ||
              card.primaryEmail.toLowerCase().indexOf(searchTerm) === 0 ||
              card.firstName.toLowerCase().indexOf(searchTerm) === 0 ||
              card.lastName.toLowerCase().indexOf(searchTerm) === 0);

            let addressRecord = "";
            if (card.displayName !== "") {
              addressRecord = `${card.displayName} <${card.primaryEmail}>`;
            } else {
              addressRecord = card.firstName;
              if (card.lastName) {
                if (addressRecord) addressRecord += " ";
                addressRecord += card.lastName;
              }

              if (addressRecord) {
                addressRecord += ` <${card.primaryEmail}>`;
              } else {
                addressRecord += card.primaryEmail;
              }
            }

            if (goodMatch) {
              matches.unshift(addressRecord);
            } else
              matches.push(addressRecord);
          }
        }

        let l = addressBook.addressLists.length;
        for (let i = 0; i < l; i++) {
          let aList = addressBook.addressLists.GetElementAt(i).QueryInterface(Ci.nsIAbDirectory);
          if (aList.dirName.toLowerCase().indexOf(searchTerm) >= 0 ||
            aList.description.toLowerCase().indexOf(searchTerm) >= 0 ||
            aList.listNickName.toLowerCase().indexOf(searchTerm) >= 0) {

            let goodMatch = (aList.dirName.toLowerCase().indexOf(searchTerm) === 0 ||
              aList.description.toLowerCase().indexOf(searchTerm) === 0 ||
              aList.listNickName.toLowerCase().indexOf(searchTerm) === 0);

            let addressRecord = `${aList.dirName} <${aList.description ? aList.description : aList.dirName}>`;
            if (goodMatch) {
              matches.unshift(addressRecord);
            } else
              matches.push(addressRecord);
          }
        }
      }
    }

    return matches;
  }

  closeAllAutocompletionLists(elmnt) {
    let acLists = document.getElementsByClassName("dropmeAutocomplete-list");
    for (let i = 0; i < acLists.length; i++) {
      while (acLists[i].itemCount > 0) {
        acLists[i].removeItemAt(0);
      }
      acLists[i].parentNode.removeChild(acLists[i]);
    }
  }

}



var DropMeDropper = {
  composeStartup: function () {
    DEBUG("dropmeComposeOverlay.js: DropMe.composeStartup()\n");

    this.contentBox = document.getElementById("composeContentBox");
    this.dropBox = document.getElementById("dropMedropBox");
    this.contentBox.addEventListener("dragenter", DropMeDropper.onDragEnter.bind(DropMeDropper));
    this.contentBox.addEventListener("dragleave", DropMeDropper.onDragLeave.bind(DropMeDropper));
    this.contentBox.addEventListener("dragover", DropMeDropper.onDragOver.bind(DropMeDropper));
    this.contentBox.addEventListener("drop", DropMeDropper.onDrop.bind(DropMeDropper));

    this.hideDropBox();
  },

  prevState: null,

  isValidEvent(event) {
    if (event.dataTransfer.types.indexOf("Files") >= 0 ||
      event.dataTransfer.types.indexOf("text/x-moz-url") >= 0) {
      return true;
    }

    return false;
  },

  onDragEnter: function (event) {
    DEBUG("dropmeComposeOverlay.js: DropMe.onDragEnter()\n");
    if (!this.isValidEvent(event)) return;
    event.preventDefault();
    this.displayDropBox();
  },

  onDragLeave: function (event) {
    DEBUG("dropmeComposeOverlay.js: DropMe.onDragLeave()\n");
    if (!this.isValidEvent(event)) return;
    event.preventDefault();
    this.hideDropBox();
  },

  onDragOver: function (event) {
    DEBUG("dropmeComposeOverlay.js: DropMe.onDragOver()\n");
    if (!this.isValidEvent(event)) return;
    this.displayDropBox();
    event.preventDefault();
  },

  onDrop: function (event) {
    DEBUG("dropmeComposeOverlay.js: DropMe.onDrop()\n");

    if (!this.isValidEvent(event)) return;

    if (event.dataTransfer !== null) {
      const dTrans = event.dataTransfer;
      let transfer = dTrans.getData("text/x-moz-url");

      if (transfer.startsWith("file://")) {

        DEBUG(`dropmeComposeOverlay.js:  handleDropEvent(${dTrans.files.length})\n`);

        for (let i = 0; i < dTrans.files.length; i++) {
          DEBUG(`dropmeComposeOverlay.js:  file: ${dTrans.files[i].mozFullPath}\n`);
          this.addFileAttachment(dTrans.files[i]);
        }
      } else if (transfer.search(/(imap|mailbox):\/\//) === 0) {
        this.addMailAttachments(dTrans);
      }
    }

    this.hideDropBox();
    try {
      // TB only
      ChangeAttachmentBucketVisibility(false);
    } catch (x) { }

    event.preventDefault();
  },

  displayDropBox: function () {
    DEBUG("dropmeComposeOverlay.js: DropMe.displayDropBox()\n");

    this.dropBox.style.display = "";
  },

  hideDropBox: function () {
    DEBUG("dropmeComposeOverlay.js: DropMe.hideDropBox()\n");

    this.dropBox.style.display = "none";
  },

  addFileAttachment: function (file) {
    const ioServ = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
    const fileObj = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);

    try {
      fileObj.initWithPath(file.mozFullPath);

      if (!fileObj.exists()) {
        fileObj.persistentDescriptor = file.mozFullPath;
      }

      let fileURI = ioServ.newFileURI(fileObj);
      let attachment = Cc["@mozilla.org/messengercompose/attachment;1"].createInstance(Ci.nsIMsgAttachment);
      attachment.url = fileURI.spec;
      attachment.size = file.size;
      attachment.contentType = file.type;
      attachment.name = file.name;

      if (typeof (AddAttachment) == "undefined") {
        // TB / Interlink / Epyrus
        AddAttachments([attachment]);
      } else {
        // SeaMonkey
        AddAttachment(attachment);
      }
    } catch (X) { }
  },

  addMailAttachments: function (transferItems) {

    for (let i = 0; i < transferItems.mozItemCount; i++) {
      let transferArr = transferItems.mozGetDataAt("text/x-moz-url", i).split(/[\r\n]+/);

      try {
        let attachment = Cc["@mozilla.org/messengercompose/attachment;1"].createInstance(Ci.nsIMsgAttachment);
        attachment.url = transferArr[0];

        if (transferArr.length > 2) {
          attachment.name = transferArr[1];
          attachment.size = Number(transferArr[2]);
        } else {
          let msg = this.getMsgFromUrl(transferArr[0]);
          if (msg) {
            attachment.url = msg.url;
            attachment.name = msg.msgHdr.subject + ".eml";
            attachment.size = msg.msgHdr.messageSize;
            attachment.contentType = "message/rfc822";
          }
        }

        if (typeof (AddAttachment) == "undefined") {
          // TB / Interlink / Epyrus
          AddAttachments([attachment]);
        } else {
          // SeaMonkey
          AddAttachment(attachment);
        }
      } catch (X) { }
    }
  },

  getMsgFromUrl: function (msgUri) {
    try {
      let messenger = Cc["@mozilla.org/messenger;1"].getService(Ci.nsIMessenger);

      let msgService = messenger.messageServiceFromURI(msgUri);
      let urlObj = {};
      msgService.GetUrlForUri(msgUri, urlObj, null);
      let url = urlObj.value.QueryInterface(Ci.nsIMsgMailNewsUrl);
      url = url.QueryInterface(Ci.nsIMsgMessageUrl).uri;

      return {
        msgHdr: messenger.messageServiceFromURI(msgUri).messageURIToMsgHdr(url),
        url: url
      };
    } catch (x) { }
    return null;
  }
};

var DropMe = {
  composeStartup: () => {
    gMsgCompose.RegisterStateListener(DropMe.composeStateListener);
    DropMeDropper.composeStartup();

  },

  composeUnload: () => {
    gMsgCompose.UnregisterStateListener(DropMe.composeStateListener);
    DropMeDropper.composeUnload();
  },

  composeStateListener: {
    NotifyComposeFieldsReady: () => {
      DropMe.toEditor = new DropmeRcptEditor("dropmeToField", "to", gMsgCompose.compFields ? gMsgCompose.compFields.to : null);
    },

    ComposeProcessDone: (aResult) => { },

    NotifyComposeBodyReady: () => { },
    SaveInFolderDone: (folderURI) => { }
  }
};

/**
 * Unload Addon for update or uninstallation
 */
DropMeDropper.composeUnload = function _unload_Dropme() {
  window.removeEventListener("unload-dropme", DropMeDropper.composeUnload, false);
  window.removeEventListener("load-dropme", DropMeDropper.composeStartup, false);

  // finally unload Addon entirely
  DropMeDropper = undefined;
  DropMe = undefined;
};

window.addEventListener("load-dropme",
  DropMe.composeStartup.bind(DropMe),
  false);

window.addEventListener("unload-dropme",
  DropMe.composeUnload.bind(DropMe),
  false);