/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


"use strict";

/*global MailServices: false, gMsgCompose: false, Recipients2CompFields: false */
/*global dump: false */

var Cc = Components.classes;
var Ci = Components.interfaces;

function DEBUG(msgStr) {
  // try {
  //   var consoleSvc = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);
  //   var scriptError = Cc["@mozilla.org/scripterror;1"].createInstance(Ci.nsIScriptError);
  //   scriptError.init(msgStr, null, null, 0, 0, scriptError.errorFlag, "DropMe");
  //   consoleSvc.logMessage(scriptError);
  // }
  // catch (ex) { }

  dump(msgStr);
}

var DropMe = {
};