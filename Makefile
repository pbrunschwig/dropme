all: eslint xpi

eslint:
	eslint *.js content/*/*.js content/*/*.jsm

xpi:
	mkdir -p build
	rm -f build/dropme-master.xpi
	zip -rD build/dropme-master.xpi * --exclude Makefile --exclude build

